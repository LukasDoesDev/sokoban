from levels import levels

BLOCK_AIR = (0, 'sprites/air.png')
BLOCK_WALL = (1, 'sprites/wall.png')
BLOCK_TARGET = (2, 'sprites/target.png')
BLOCK_PLAYER = (3, 'sprites/player.png')
BLOCK_BOX = (4, 'sprites/box.png')

BLOCK_TARGET_BOX = (5, 'sprites/box_target.png')
BLOCK_TARGET_PLAYER = (6, 'sprites/player_target.png')

BLOCK_SPRITES_NAMES = {}
for block_id, block_sprite in [
    BLOCK_AIR,
    BLOCK_WALL,
    BLOCK_TARGET,
    BLOCK_PLAYER,
    BLOCK_BOX,
    BLOCK_TARGET_BOX,
    BLOCK_TARGET_PLAYER
    ]:
    BLOCK_SPRITES_NAMES.setdefault(block_id, block_sprite)

BLOCK_SIZE = 64


def width_fn(lvl):
    return max(map(lambda row: len(row),lvl))
max_win_width = max(map(width_fn, levels)) * 64
max_win_height = max(map(lambda lvl: len(lvl), levels)) * 64

max_win_size = max(max_win_width, max_win_height)

print()
print()
print('Window size', max_win_size)
print()
print()



WINDOW_SIZE = (max_win_width, max_win_height)
