import os

LEVEL_DIRECTORY = './levels'

levels = []

def load_levels():
    print('Loading all levels...')
    global levels
    levels = []
    for level_filename in sorted(os.listdir(LEVEL_DIRECTORY)):
        with open(os.path.join(LEVEL_DIRECTORY, level_filename), 'r') as f:
            lines = f.read().splitlines()
            levels.append(list(map(
                lambda line: list(map(
                    lambda char: int(char),
                    line
                )),
                lines
            )))
    return levels

load_levels()
