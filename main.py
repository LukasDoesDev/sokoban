from levels import levels, load_levels
from constants import *
import pygame
import math

# Initialize pygame
pygame.init()

# Create and setup the screem
win = pygame.display.set_mode(WINDOW_SIZE, pygame.RESIZABLE)
screen = pygame.Surface(WINDOW_SIZE)
pygame.display.set_caption('Sōko-ban')
icon = pygame.image.load('sprites/player_raw.png')
pygame.display.set_icon(icon)

current_level = levels[0]
current_level_id = 0

def convert_sprite(name):
    return pygame.transform.scale(
        pygame.image.load(name),
        (
            BLOCK_SIZE,
            BLOCK_SIZE
        )
    )
block_sprites = {k: convert_sprite(v) for k, v in BLOCK_SPRITES_NAMES.items()}

class Player:
    def __init__(self):
        self.player_img = convert_sprite('sprites/player.png')
        self.player_target_img = convert_sprite('sprites/player_target.png')
        self.player_pos = (0,0)

    def get_position(self):
        for column_index, column in enumerate(current_level):
            for row_index, row in enumerate(column):
                if row in [BLOCK_TARGET_PLAYER[0], BLOCK_PLAYER[0]]:
                    self.player_pos = (row_index, column_index)
                    return
        raise Exception('ERROR')

    def set_level(self, level_id = None):
        global levels
        global current_level
        global current_level_id
        if level_id == None:
            level_id = current_level_id + 1
        try:
            current_level = levels[level_id]
            current_level_id = level_id
            self.get_position()
            print('Set level to', level_id)
        except IndexError:
            print(f'Level is {level_id - 1}, warpping back to 0')
            levels = load_levels()
            self.set_level(0)
    
    def check_can_move(self, x, y):        
        if current_level[y][x] not in [BLOCK_AIR[0], BLOCK_TARGET[0], BLOCK_BOX[0], BLOCK_TARGET_BOX[0]]:
            print('Not in list of movable objects')
            return False # Not in list of movable objects
        
        return True
    
    def move(self, relative_pos):
        rel_x, rel_y = relative_pos
        og_x, og_y = self.player_pos
        x, y = og_x + rel_x, og_y + rel_y

        if not self.check_can_move(x, y):
            return
        
        if current_level[y][x] in [BLOCK_BOX[0], BLOCK_TARGET_BOX[0]]:
            # pushing a box
            if current_level[y + rel_y][x + rel_x] not in [BLOCK_AIR[0], BLOCK_TARGET[0]]:
                return # trying to move a box into a non-air block
            
            pushing_into_type = current_level[y + rel_y][x + rel_x]
            current_level[y + rel_y][x + rel_x] = (
                pushing_into_type == BLOCK_TARGET[0] and \
                BLOCK_TARGET_BOX[0] or BLOCK_BOX[0]
            )
        
        self.player_pos = x, y
        
        og_type = current_level[og_y][og_x]
        current_level[og_y][og_x] = (
            og_type == BLOCK_TARGET_PLAYER[0] and \
            BLOCK_TARGET[0] or BLOCK_AIR[0]
        )
        new_type = current_level[y][x]
        current_level[y][x] = (
            new_type in [BLOCK_TARGET_BOX[0], BLOCK_TARGET[0]] and \
            BLOCK_TARGET_PLAYER[0] or BLOCK_PLAYER[0]
        )

        self.check_if_finished()
    
    def check_if_finished(self):
        for column in current_level:
            for row in column:
                if row == BLOCK_BOX[0]:
                    return
        self.set_level()


player = Player()
player.get_position()

def draw_level():
    lvl_y = len(current_level) - 1
    lvl_x = len(current_level[0]) - 1
    
    win.fill((255,255,255))
    for win_x in range(0, math.ceil(screen.get_size()[0] / BLOCK_SIZE)):
        for win_y in range(0, math.ceil(screen.get_size()[1] / BLOCK_SIZE)):
            if win_x > lvl_x or win_y > lvl_y:
                screen.blit(block_sprites[BLOCK_AIR[0]], (win_x * BLOCK_SIZE, win_y * BLOCK_SIZE))
    
    for column_index, column in enumerate(current_level):
        for row_index, row in enumerate(column):
            x, y = row_index * BLOCK_SIZE, column_index * BLOCK_SIZE
            screen.blit(block_sprites[row], (x, y))

# Game loop
running = True
while running:
    win.fill((255,255,255))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                player.move((0, -1))
            elif event.key == pygame.K_RIGHT:
                player.move((1, 0))
            elif event.key == pygame.K_DOWN:
                player.move((0, 1))
            elif event.key == pygame.K_LEFT:
                player.move((-1, 0))

    draw_level()
    win_width, win_height = win.get_size()
    if win_width > win_height:
        # Landscape
        scaled_win = pygame.transform.scale(screen, (win_height, win_height))
        blit_pos = (win_width / 2 - scaled_win.get_size()[0] / 2, 0)
        win.blit(scaled_win, blit_pos)
    else:
        # Portrait
        scaled_win = pygame.transform.scale(screen, (win_width, win_width))
        blit_pos = (0, win_height / 2 - scaled_win.get_size()[1] / 2)
        win.blit(scaled_win, blit_pos)
    
    pygame.display.update()
